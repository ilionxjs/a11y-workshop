Opdracht Accessibility Screen Reader

# Opdracht

## Benodigdheden: Screen Reader
We raden NVDA aan als screenreader om te installeren. Deze is gratis en kan je blijven gebruiken.
Op Apple machines staat al de applicatie VoiceOver geinstalleerd.
www.nvaccess.com

### Voordat je begint:
Kies een Nederlandse stem in de screen reader software die je prettig vind klinken. Ook een lagere opleessnelheid is aan te raden zodat het beter te volgen is.

Als je vastloopt op een onderdeel, vraag dan de persoon waarmee je samenwerkt om hulp.

Tijdens de uitvoer, overleg in de groep wat er beter zou kunnen aan de website om het meer accessible te maken.

## Uitvoer
- 2/3 mensen aan 1 laptop.
- 1 screenreader gebruiker, mag het scherm niet zien.
- De ander noteert wat er fout gaat.

### aan de knoppen:
probeer een beeld te vormen door de headings / landmarks bij langs te gaan.

- zoek naar invoer formulieren
- gebruik de hotkey (CapsLock) voor stoppen text
- je zal vaak overnieuw moeten
- het hoeft niet af
- je hebt de tijd

### noteren wat er fout ging:
- hoe vaak ging hij/zij overnieuw? wat was de oorzaak hiervan?
- op welke plekken bleef hij/zij hangen?
- wat zou verbetert kunnen worden aan de website om het meer accessible te maken?

### Korte cheatsheet NVDA
Neem het cheatsheet door om een beetje bekend te raken met de controls van NVDA.
De handigste acties zijn:

- volgende / vorige Heading: H / shift+H
- volgende / vorige Landmark: L / shift+L
- volgende / vorige Form: F / shift+F
- volgende / vorige Link: K / shift+K

Met deze shortcuts probeer je je een beeld te vormen van de website.

## vragen
1. Welke films zijn genomineerd voor de oscar Beste Film?
2. Hoe laat vertrekt de volgende trein naar amsterdam? (en hoe laat kom je aan)
3. Hoe duur is de Google Home Mini op bol.com?
4. Hoe is je favoriete website via een screenreader?
5. ?????

# extras
1. Boek een vliegreis op expedia.com (tot aan checkout)
2. Bestel eten op thuisbezorgd.nl (tot aan checkout)
3. Koop iets op bol.com (tot aan checkout)
4. Vind de goedkoopste monitor op tweakers.net


